{{--
  ./resources/views/posts/show.blade.php
  vairables disponibles :
      - $posts Post
 --}}
@extends('template.app')

@section('title')
  {{ $post->title }}
@endsection

@section('content1')
  <div class="col-md-12 blog-post">
    <!-- Post Headline Start -->
    <div class="post-title">
      <h1>{{ $post->title }}</h1>
    </div>
    <!-- Post Headline End -->

    <!-- Post Detail Start -->
    <div class="post-info">
      <span>{{ $post->created_at }}</span><br/>
      <span>
        Author:
        <a href="{{ route('users.show', [
                          'user'   => $post->userData->id,
                          'pseudo' => Str::slug($post->userData->pseudo, '-')
                          ])}}">
              {{ $post->userData->pseudo }}
        </a>
      </span>
      <span>Category:
        <a href="{{ route('categories.show', [
              'categorie' => $post->categorie->id,
              'slug'      => Str::slug($post->categorie->name, '-')
          ]) }}">
          {{ $post->categorie->name }}
        </a>
      </span>
      <ul>
        @foreach ($post->tags as $tag)
          <li>
            <a href="{{ route('tags.show', [
                    'tag' => $tag->id,
                    'slug' => Str::slug($tag->name, '-')
              ]) }}">
              {{ $tag->name }}</a>
          </li>
        @endforeach
      </ul>
    </div>
    <!-- Post Detail End -->

     {!! html_entity_decode($post->text) !!}

    <!-- Post Blockquote (Italic Style) Start -->
    <blockquote class="margin-top-40 margin-bottom-40">
      {{ $post->cite }}
    </blockquote>
    <!-- Post Blockquote (Italic Style) End -->

    <!-- Post Buttons -->
    <div>
      <a href="form.html" type="button" class="btn btn-primary">Edit Post</a>
      <a href="#" type="button" class="btn btn-secondary" role="button">Delete Post</a>
    </div>
    <!-- Post Buttons End -->

    </div>
@endsection
