{{--
  ./resources/views/posts/index.blade.php
  variables disponibles :
      - $posts ARRAY Post
 --}}
@extends('template.app')

@section('title')
  Liste des posts
@endsection

@section('content1')
  <!-- ADD A POST -->
  <div>
    <a href="#" type="button" class="btn btn-primary">Add a Post</a>
  </div>
  <!-- ADD A POST END -->

  @foreach ($posts as $post)
    <!-- Blog Post Start -->
    <div class="col-md-12 blog-post">
      <div class="post-title">
        <a href="{{ URL::route('posts.show',
                    ['post' => $post->id, 'slug' => $post->slug])
                  }}">
          <h1>{{ $post->title }}</h1>
        </a>
      </div>
      <div class="post-info">
        <span>{{ \Carbon\Carbon::parse($post->created_at)->format('d-m-Y') }}</span><br/>
        <span>Author: {{ $post->userData->pseudo }}</span><br/>
        <span>Category:
          <a href="{{ route('categories.show', [
                'categorie' => $post->categorie->id,
                'slug' => Str::slug($post->categorie->name)
            ]) }}">
            {{ $post->categorie->name }}
          </a>
        </span>
      </div>
      <p>{{ Str::words($post->text, 100, '...') }}</p>
      <a href="{{ URL::route('posts.show', ['post' => $post->id, 'slug' => $post->slug]) }}" class="button button-style button-anim fa fa-long-arrow-right">
        <span>Read More</span>
      </a>
    </div>
    <!-- Blog Post End -->
  @endforeach

@endsection
