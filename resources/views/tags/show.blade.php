{{--
  ./resources/views/tags/show.blade.php
  vairables disponibles :
      - $tag Tag
 --}}
@extends('template.app')

@section('title')
  Posts de {{ $tag->name }}
@endsection

@section('content1')
  <div class="col-md-12 blog-post">
    <!-- Post Headline Start -->
      <div class="post-title">
        <h1>{{ $tag->name }}</h1>
      </div>
    <!-- Post Headline End -->

    <!-- Post Detail Start -->
      <div class="post-info">
        <span>
          Liste des posts
        </span>
        <ul>
          @foreach ($tag->posts as $post)
            <li>
              <a href="{{ route('posts.show', [
                'post' => $post->id,
                'slug' => $post->slug
                ]) }}">
                {{ $post->title }}
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    <!-- Post Detail End -->

    </div>
@endsection
