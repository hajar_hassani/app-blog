{{--
  ./resources/views/tags/show.blade.php
  variables disponibles :
      - $categorie Categorie
 --}}
@extends('template.app')

@section('title')
  Posts du tag {{ $categorie->name }}
@endsection

@section('content1')
  <div class="col-md-12 blog-post">
    <!-- Post Headline Start -->
      <div class="post-title">
        <h1>{{ $categorie->name }}</h1>
      </div>
    <!-- Post Headline End -->

    <!-- Post Detail Start -->
      <div class="post-info">
        <span>
          Liste des posts
        </span>
        <ul>
          @foreach ($categorie->posts as $post)
            <li>
              <a href="{{ route('posts.show', [
                  'post' => $post->id,
                  'slug' => $post->slug
                ]) }}">
                {{ $post->title }}
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    <!-- Post Detail End -->

    </div>
@endsection
