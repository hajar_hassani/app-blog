{{--
  ./resources/views/tags/create.blade.php
 --}}
@extends('template.app')

@section('title')
   Ajouter une catégorie
@endsection

@section('content1')
  <div class="col-md-12 blog-post">
    <!-- Post Headline Start -->
      <div class="post-title">
        <h1>Ajouter une catégorie</h1>
      </div>
    <!-- Post Headline End -->

    <!-- Post Detail Start -->
      <div class="post-info">
        <form class="" action="{{ route('categories.store') }}" method="post">
          @csrf
          <div class="">
            <label for="name">Name</label>
            <input type="text" id="name" name="name" />
          </div>
          <div><input type="submit" /></div>
        </form>
      </div>
    <!-- Post Detail End -->

    </div>
@endsection
