{{--
  ./resources/views/tags/edit.blade.php
  variables disponibles :
      - $categorie Categorie
 --}}
@extends('template.app')

@section('title')
  Edition d'une catgeorie
@endsection

@section('content1')
  <div class="col-md-12 blog-post">
    <!-- Post Headline Start -->
      <div class="post-title">
        <h1>{{ $categorie->name }}</h1>
      </div>
    <!-- Post Headline End -->

    <!-- Post Detail Start -->
      <div class="post-info">
        <form action="{{ route('categories.update', ['categorie' => $categorie->id]) }}" method="post">
          @csrf
          {{ method_field('PATCH') }}
          <div class="">
            <label for="name">Name</label>
            <input type="text" id="name" name="name" value="{{ $categorie->name }}" />
          </div>
          <div><input type="submit" /></div>
        </form>
      </div>
    <!-- Post Detail End -->

    </div>
@endsection
