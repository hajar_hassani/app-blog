{{--
  ./resources/views/categories/index.blade.php
  variables disponibles :
      - $categories Array(Categorie)
 --}}
@extends('template.app')

@section('title')
  Liste des catégories
@endsection

@section('content1')
  <div class="col-md-12 blog-post">
    <!-- Post Headline Start -->
      <div class="post-title">
        <h1>Liste des catégories</h1>
      </div>
    <!-- Post Headline End -->

    <!-- Post Detail Start -->
      <div class="post-info">
        <ul>
          @foreach ($categories as $categorie)
            <li>
              <a href="{{ route('categories.show', [
                  'categorie' => $categorie->id,
                  'slug' => Str::slug($categorie->name)
                ]) }}">
                {{ $categorie->name }}
              </a> [
                      <a href="{{ route('categories.edit', [
                        'categorie' => $categorie->id
                        ]) }}">
                        Edit
                      </a> |
                      <a href="{{ route('categories.destroy', [
                        'categorie' => $categorie->id
                        ]) }}" data-method="delete">
                        Delete
                      </a>
                   ]

            </li>
          @endforeach
        </ul>
      </div>
    <!-- Post Detail End -->

    </div>
@endsection
