{{--
  ./resources/views/categories/menu.blade.php
  variables disponibles :
      - $categories Array(Categorie)
 --}}
<h1>Liste des catégories</h1>
<ul>
  @foreach ($categories as $categorie)
    <li>
      <a href="#">
        {{ $categorie->name }}
      </a>
    </li>
  @endforeach
</ul>
