<div class="about-fixed">

  <div class="my-pic">
     <img src="images/pic/my-pic.png" alt="">
      <nav id="menu" class="collapsed">
        <ul class="menu-link">
          <li><a href="{{ URL::route('posts.index') }}">My Blog</a></li>
          <li> | </li>
          <li><a href="{{ URL::route('categories.create') }}">Add a category</a></li>
         </ul>
      </nav>
 </div>



   <div class="my-detail">

     <div class="white-spacing">
         <h1>Alex Parker</h1>
         <span>Web Developer</span>
     </div>
     
     <div class="white-spacing">
         {{--
         @include(
           'categories._menu',
           ['categories' => \App\Http\Models\Categorie::all()]
             //Comment charger un fichier dans le template
             //et je lui envoie les infos dont il a besoin
           )
         --}}
        @include('categories._menu')
     </div>

    <ul class="social-icon">
      <li><a href="#" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
      <li><a href="#" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
      <li><a href="#" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
      <li><a href="#" target="_blank" class="github"><i class="fa fa-github"></i></a></li>
     </ul>

 </div>
</div>
