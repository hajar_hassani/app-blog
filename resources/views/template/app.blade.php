<!DOCTYPE html>
<html lang="en">
  <head>
    @include('template.partials._head')
  </head>
 <body>
	  <!-- Preloader Start -->
      @include('template.partials._preloader')
    <!-- Preloader End -->

    <div id="main">
        <div class="container">
            <div class="row">

                 <!-- About Me (Left Sidebar) Start -->
                   <div class="col-md-3">
                     @include('template.partials._header')
                   </div>
                <!-- About Me (Left Sidebar) End -->

                 <!-- Blog Post (Right Sidebar) Start -->
                   <div class="col-md-9">
                      <div class="col-md-12 page-body">
                      	<div class="row">
                              <div class="col-md-12 content-page">
                                @yield('content1')
                               </div>
                           </div>
                        </div>

                         <!-- Footer Start -->
                         <div class="col-md-12 page-body margin-top-50 footer">
                            @include('template.partials._footer')
                         </div>
                         <!-- Footer End -->

                    </div>
                  <!-- Blog Post (Right Sidebar) End -->

            </div>
         </div>
      </div>

    <!-- Back to Top Start -->
      @include('template.partials._back_to_top')
    <!-- Back to Top End -->

    <!-- All Javascript Plugins  -->
      @include('template.partials._scripts')
   </body>
 </html>
