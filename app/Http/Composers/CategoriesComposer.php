<?php
namespace App\Http\Composers;
use Illuminate\View\View;
use \App\Http\Models\Categorie;

class CategoriesComposer {

    /**
     * @param  View  $view
     * @return void
     */
    public function menu(View $view) {
        $view->with('categories', Categorie::all());
    }
}
