<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
  /**
   * Get the posts for the user.
   */
  public function posts() {
      return $this->hasMany('App\Http\Models\Post', 'user');
  }
}
