<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class Post extends Model {
  /**
   * Get the post that owns the comment.
   */
  public function userData(){
      return $this->belongsTo('App\Http\Models\User', 'user');
  }

  /**
   * Get the categorie that owns the post.
   */
  public function categorie(){
      return $this->belongsTo('App\Http\Models\Categorie', 'category');
  }
  
  /**
   * Get the tags of the post.
   */
  public function tags() {
    return $this->belongsToMany('App\Http\Models\Tag', 'posts_has_tags', 'post', 'tag');
  }

}
