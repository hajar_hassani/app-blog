<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model {

  protected $fillable = ['name'];

  /**
   * Get the posts for the categorie.
   */
  public function posts() {
      return $this->hasMany('App\Http\Models\Post', 'category');
  }
}
