<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {
  /**
   * Get the posts of the tag.
   */
  public function posts() {
    return $this->belongsToMany('App\Http\Models\Post', 'posts_has_tags', 'post', 'tag');
  }
}
