<?php
   namespace App\Http\Controllers;

   use Illuminate\Support\Facades\View;
   use App\Http\Models\Categorie;

   class CategoriesController extends Controller {

     public function index() {
       $categories = Categorie::all();
       return View::make('categories.index', compact('categories'));
     }

      public function show(Categorie $categorie) {
        return View::make('categories.show', compact('categorie'));
      }

      public function create() {
        return View::make('categories.create');
      }

      public function store(Request $resquest) {
        Categorie::create($request->all());   // Il va récupérer toutes les données
        return redirect()>route('categories.index');
      }

      public function edit(Categorie $categorie) {
        return View::make('categories.edit', compact('categorie'));
      }

      public function update(Request $request, Categorie $categorie) {
        $categorie->update($request->all());
        return redirect()->route('categories.index');
      }

      public function destroy(Categorie $categorie) {
        $categorie->destroy();
        return redirect()->route('categories.index');
      }
   }
