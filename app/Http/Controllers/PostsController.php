<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Http\Models\Post;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller {

  public function index(){
    $posts = Post::all();
    return View::make('posts.index',['posts' => $posts]);
  }

  public function show(Post $post, string $slug){
    return View::make('posts.show',compact('post'));
  }

}
