<?php
   namespace App\Http\Controllers;

   use Illuminate\Support\Facades\View;
   use App\Http\Models\Tag;

   class TagsController extends Controller {

      public function show(Tag $tag) {
        return View::make('tags.show', compact('tag'));
      }
   }
