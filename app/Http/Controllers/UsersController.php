<?php
   namespace App\Http\Controllers;

   use Illuminate\Support\Facades\View;
   use App\Http\Models\User;

   class UsersController extends Controller {

      public function show(User $user) {
        return View::make('users.show', compact('user'));
      }
   }
