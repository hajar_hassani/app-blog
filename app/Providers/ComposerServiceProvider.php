<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        // VERSION 1: avec un callback
          View::composer('categories._menu', function($view){
            $view->with('categories', \App\Http\Models\Categorie::all());
          });


        // VERSION 3: appel d'une classe et d'une méthode 'menu'
        //  View::composer('categories._menu', '\App\Http\Composers\CategoriesComposer@menu');
    }
}
