<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
----------------------------------------------------------------------------
VIEW COMPOSERS:
Mise à disposition de données pour certaines vues
*/
/*Dès qu'on charge la vue menu il fait ce qu'on lui demande ensuite*/
// View::composer('categories._menu', function($view){
//   $view->with('categories', \App\Http\Models\Categorie::all());
// });

// View::composer(
// 'categories._menu',
// '\App\Http\Composers\CategoriesComposer@menu'
// );
/*
----------------------------------------------------------------------------
*/

/*
  DETAILS D'UN POST
  PATTERN: /posts/id/slug.html
  CTRL: Posts
  ACTION: show
 */
 Route::get('/posts/{post}/{slug}.html', 'PostsController@show')
     ->where([
               'post'   => '[1-9][0-9]*',
               'slug' => '[a-z0-9][a-z0-9\-]*'
             ])
     ->name('posts.show');

/*
   DETAILS D'UN USER
   PATTERN: /users/id/pseudo.html
   CTRL: Users
   ACTION: show
*/
Route::get('/users/{user}/{pseudo}.html','UsersController@show')
      ->where([
            'user' => '[1-9][0-9]*',
            'pseudo' => '[a-z0-9][a-z0-9\-]*'
      ])
      -> name('users.show');
/*
   DETAILS D'UN TAG
   PATTERN: /tags/id/tag.html
   CTRL: Tags
   ACTION: show
*/
Route::get('/tags/{tag}/{slug}.html','TagsController@show')
      ->where([
            'tag' => '[1-9][0-9]*',
            'slug' => '[a-z0-9][a-z0-9\-]*'
      ])
      -> name('tags.show');

/*
  DETAILS D'UNE CATEGORIE
  PATTERN: /categories/id/slug.html
  CTRL: Categories
  ACTION: show
 */
  Route::get('/categories/{categorie}/{slug}.html','CategoriesController@show')
        ->where([
             'categorie' => '[1-9][0-9]*',
             'slug' => '[a-z0-9][a-z0-9\-]*'
        ])
        ->name('categories.show');

  Route::resource('categories', 'CategoriesController');

/*
  FORMULAIRE D'AJOUT D'UNE CATEGORIE
  PATTERN: /categories/create
  CTRL: Categories
  ACTION: create
 */
 Route::get('/categories/create','CategoriesController@create')
       ->name('categories.create');

/*
 FORMULAIRE D'EDITION D'UNE CATEGORIE
 PATTERN: /categories/{categorie}/edit
 CTRL: Categories
 ACTION: edit
*/
Route::get('/categories/{categorie}/edit','CategoriesController@edit')
      ->where([
           'categorie' => '[1-9][0-9]*',
      ])
      ->name('categories.edit');

/*
 UPDATE D'UNE CATEGORIE
 PATTERN: /categories/{categorie} [PATCH]
 CTRL: Categories
 ACTION: update
*/
Route::patch('/categories/{categorie}','CategoriesController@update')
      ->where([
           'categorie' => '[1-9][0-9]*',
      ])
      ->name('categories.update');

/*
 SUPPRESSION D'UNE CATEGORIE
 PATTERN: /categories/{categorie} [DELETE]
 CTRL: Categories
 ACTION: destroy
*/
Route::delete('/categories/{categorie}','CategoriesController@destroy')
      ->where([
           'categorie' => '[1-9][0-9]*',
      ])
      ->name('categories.destroy');

/*
 INSERT D'UNE CATEGORIE
 PATTERN: /categories [POST]
 CTRL: Categories
 ACTION: store
*/
Route::post('/categories','CategoriesController@store')
      ->name('categories.store');

/*
 LISTE D'UNE CATEGORIE
 PATTERN: /categories [GET]
 CTRL: Categories
 ACTION: index
*/
Route::get('/categories','CategoriesController@index')
      ->name('categories.index');

/*
  ROUTE PAR DEFAUT
  PATTERN: /
  CTRL: Produits
  ACTION: index
    > Dans content1: ul li avec la liste des noms de produits
    > Dans title: Nos produits
 */
 Route::get('/', 'PostsController@index')->name('posts.index');
